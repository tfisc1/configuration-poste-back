package tpRecette.service;

import java.util.List;

import tpRecette.model.Recette;

public interface RecetteService {
	
	List<Recette> buildListRecette(List<Long> listId);

}
