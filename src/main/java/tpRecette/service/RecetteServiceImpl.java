package tpRecette.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tpRecette.dao.RecetteDao;
import tpRecette.model.Recette;

@Service
public class RecetteServiceImpl implements RecetteService {
	
	@Autowired
	private RecetteDao recetteDao;
	
	@Override
	public List<Recette> buildListRecette(List<Long> listId)
	{
		List<Recette> listRecetteAExporter = new ArrayList<>();
		for(Long id : listId)
		{
			Optional<Recette> recette = recetteDao.findById(id);
			listRecetteAExporter.add(recette.get());
		}
		return listRecetteAExporter;
	}
	
	
}
