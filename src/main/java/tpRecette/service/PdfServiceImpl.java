package tpRecette.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import tpRecette.dao.RecetteDao;
import tpRecette.model.Recette;

@Service
public class PdfServiceImpl implements PdfService {
	
	@Autowired
	private RecetteDao recetteDao;
	
	private static final String JRXML_PATH = "src\\main\\resources\\templates";
	
	private static final String PDF_PATH = "C:\\Users\\Jean\\Desktop\\java\\pdf";
	
	
	public byte[] genererPdf(List<Recette> listRecette)
	{
		JasperReport jasperReport;
		byte[] bytes = null;
		try {
			jasperReport = JasperCompileManager.compileReport(JRXML_PATH+"\\recette-rpt.jrxml");
			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(listRecette);
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("Crée par", "Moi");
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
					jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, PDF_PATH + "\\recette-Rpt.pdf");
			bytes = JasperExportManager.exportReportToPdf(jasperPrint);
			return bytes;
		} catch (JRException e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public byte[] getPdf(List<Recette> listRecette) 
	{
		return genererPdf(listRecette);
	}
	
}
