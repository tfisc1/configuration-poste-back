package tpRecette.service;

import java.util.List;

import tpRecette.model.Recette;

public interface PdfService {

	byte[] getPdf(List<Recette> listRecette);
}
