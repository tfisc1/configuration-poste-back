package tpRecette;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/*import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;*/

@SpringBootApplication
public class TpRecetteApplication /*extends SpringBootServletInitializer */{
	
	private static final Logger logger = LogManager.getLogger(TpRecetteApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(TpRecetteApplication.class, args);
		
		logger.info("Info level log message");
		logger.debug("Debug level log message");
		logger.error("Error level log message");
	}
	
	/*@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
	{
		return application.sources(TpRecetteApplication.class);
	}*/
	
}
