package tpRecette.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import tpRecette.model.Recette;

public interface RecetteDao extends	JpaRepository<Recette, Serializable> {
}
