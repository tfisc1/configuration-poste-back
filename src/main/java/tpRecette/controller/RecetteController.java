package tpRecette.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tpRecette.dao.RecetteDao;
import tpRecette.model.Recette;

@RestController
@CrossOrigin(origins = "http://localhost")
@RequestMapping("/api/recette")
public class RecetteController {

	@Autowired
	private RecetteDao	recetteDao;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<Recette> list()
	{
		return this.recetteDao.findAll();
	}
}
