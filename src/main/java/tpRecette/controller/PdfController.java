package tpRecette.controller;

import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tpRecette.dao.RecetteDao;
import tpRecette.service.PdfService;
import tpRecette.service.RecetteService;

@RestController
@CrossOrigin(origins = "http://localhost")
@RequestMapping("/api/pdf")
public class PdfController {
	
	@Autowired
	private PdfService 	pdfService;
	
	@Autowired
	private RecetteDao	recetteDao;
	
	@Autowired
	private RecetteService	recetteService;

	@RequestMapping(method = RequestMethod.GET , value="/all")
	public byte[] getAllPdf()
	{
	    return pdfService.getPdf(this.recetteDao.findAll());
	}
	
	@RequestMapping(method = RequestMethod.POST , value="/allById")
	@ResponseBody
	public ResponseEntity<byte[]> getAllPdfByIdSelectionnes(@RequestBody List<Long> listId)
	{
		String username = "recette-rpt";
		byte[] bytes = pdfService.getPdf(recetteService.buildListRecette(listId));
		//String base64 = Base64.getEncoder().encodeToString(bytes);
	    return ResponseEntity
	    	      .ok()
	    	      .header("Content-Type", "application/pdf; charset=UTF-8")
	    	      .header("Content-Disposition", "inline; filename=\"" + username + ".pdf\"")
	    	      .body(bytes);
	}
	
	
}
