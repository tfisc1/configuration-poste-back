FROM java:8

ADD target/TpRecette-0.0.1-SNAPSHOT.jar TpRecette-0.0.1-SNAPSHOT.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "TpRecette-0.0.1-SNAPSHOT.jar"]



